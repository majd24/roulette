import java.util.Scanner;
public class Roulette {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        RouletteWheel myRouletteWheel = new RouletteWheel();
        int cash = 1000;
        boolean gameOver = false;
        while(!gameOver) {
            System.out.println("Hi user! Do you want to a make a bet or not.. Type \"Y\" for Yes or \"N\" for No..");

            String userResponse = reader.next();
            if(userResponse.equals("Y")) {
                System.out.println("what is the amount of money you want to bet on?");
                int userAmountOfMoney = reader.nextInt();
                System.out.print("what number you want to bet on? Please choose between 0 and 36? ");
                int userBet = reader.nextInt();
                if(userAmountOfMoney > cash) {
                    System.out.println("You do not have enough money to bet.. Please try again!");
                    continue;
                }
                else {
                    myRouletteWheel.spin();
                    System.out.println("You choose: " + userBet);
                    System.out.println("The roulette number is: " + myRouletteWheel.getValue());
                    if(userBet == myRouletteWheel.getValue()) {
                        cash = cash + (userAmountOfMoney * 35);
                    }
                    else {
                        cash = cash -  userAmountOfMoney;
                    }
                    if(cash <= 0) {
                        System.out.println("You have $0 to bet.. Game Over!");
                        break;
                    }
                }
            }
            else {
                gameOver = true;
            }
        }
        System.out.println("You have $" + cash + " in your pocket!");
        reader.close();
    }
}